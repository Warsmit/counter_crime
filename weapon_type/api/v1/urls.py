from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .viewsets import  (WeaponTypeList, WeaponTypeDetail)

urlpatterns = [
    url(r'^weapon_types/$', WeaponTypeList.as_view(), name='weapontype-list'),
    url(r'^weapon_types/(?P<pk>[0-9]+)$', WeaponTypeDetail.as_view(), name='weapontype-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
