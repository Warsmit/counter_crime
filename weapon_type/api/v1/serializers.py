from rest_framework import serializers
from weapon_type.models import WeaponType

class WeaponTypeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = WeaponType
        fields = '__all__'