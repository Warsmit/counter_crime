from django.db import models

class WeaponType(models.Model):
    """
    Model for storing weapon types
        fields:
            name -> type weapon name
            created -> date and time of creation
            updated -> date and time of updation

    """
    class Meta:
        db_table = "WeaponType"
        verbose_name = "Тип Оружия"
        verbose_name_plural = "Типы Оружий"
        permissions = (
            ("view_weapon_type", "Can view Типы Оружий"),
        )

    name = models.CharField(verbose_name="Тип Оружия", blank=False, null=False, max_length=100)
    created = models.DateTimeField(verbose_name="Создано", auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(verbose_name="Обновлено", auto_now_add=False, auto_now=True)

    def __str__(self):
        return "{NAME}".format(
            NAME=self.name,
        )
