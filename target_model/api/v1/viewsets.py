from .serializers import TargetModelSerializer
from target_model.models import Target
from user.permissions import ExtendedModelPermissionsNonAuthReadOnly
from rest_framework.filters import SearchFilter
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import (
    generics,
    permissions
)


class TargetModelList(generics.ListCreateAPIView):
    queryset = Target.objects.all()
    serializer_class = TargetModelSerializer

    filter_backends = (
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter,
    )

    filter_fields = '__all__'
    search_fields = '__all__'
    ordering_fields = '__all__'

    permission_classes = [
        # permissions.IsAuthenticatedOrReadOnly,
        ExtendedModelPermissionsNonAuthReadOnly
        # permissions.DjangoModelPermissionsOrAnonReadOnly,
        # ExtendedModelPermissions
    ]


class TargetModelDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Target.objects.all()
    serializer_class = TargetModelSerializer

    permission_classes = [
        # permissions.IsAuthenticated,
        # permissions.IsAuthenticatedOrReadOnly,
        ExtendedModelPermissionsNonAuthReadOnly
        # ExtendedModelPermissions
    ]