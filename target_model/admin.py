from django.contrib import admin
from .models import Target

class TargetModelAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Target._meta.fields]

    class Meta:
        model = Target

admin.site.register(Target, TargetModelAdmin)
