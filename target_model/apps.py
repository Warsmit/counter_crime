from django.apps import AppConfig


class TargetModelConfig(AppConfig):
    name = 'target_model'
