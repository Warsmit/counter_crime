from rest_framework import serializers
from ammo_type.models import AmmoType

class AmmoTypeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AmmoType
        fields = '__all__'
