import os
import requests
import json

sever_url = "http://127.0.0.1:90/"
client = requests.session()


def upload_data(data, url):
    for d in data:
        d = json.dumps(d)
        print("uploading: {}".format(d))
        result = client.post(url, data=d, headers=headers,
                             cookies=client.cookies)
        print(result.json())


ammo_type_url = sever_url + "api/v1/ammo_types/"


ammo_type = [
    {
        "caliber": "8,70—9,25",
        "marker": "хз че это",
    }
]

upload_data(ammo_types, ammo_type_url)