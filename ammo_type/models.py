from django.db import models

class AmmoType(models.Model):
    """
    Model for storing different types of ammo
        fields:
            caliber -> caliber type
            marker -> marker type
            count -> number of ammo
            created -> date and time of creation
            updated -> date and time of updation

    """
    class Meta:
        db_table = "AmmoType"
        verbose_name = "Тип боеприпасов"
        verbose_name_plural = "Типы боеприпасов"
        permissions = (
            ("view_ammo_type", "Can view Типы боеприпасов"),
        )

    caliber = models.CharField(verbose_name="Калибр", blank=False, null=False, max_length=30)
    marker = models.CharField(verbose_name="Маркер", blank=False, null=False, max_length=100)
    count = models.PositiveIntegerField(verbose_name="Количество патронов")
    created = models.DateTimeField(verbose_name="Создано", auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(verbose_name="Обновлено", auto_now_add=False, auto_now=True)

    def __str__(self):
        return '{CALIBER} - {COUNT} штук, {MARKER}'.format(
            CALIBER=self.caliber,
            COUNT=self.count,
            MARKER=self.marker,
        )