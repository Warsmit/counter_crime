from django.contrib import admin
from .models import AmmoType

class AmmoTypeAdmin(admin.ModelAdmin):
    list_display = [field.name for field in AmmoType._meta.fields]

    class Meta:
        model = AmmoType

admin.site.register(AmmoType, AmmoTypeAdmin)
