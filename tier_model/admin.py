from django.contrib import admin
from .models import TierModel

class TierModelAdmin(admin.ModelAdmin):
    list_display = [field.name for field in TierModel._meta.fields]

    class Meta:
        model = TierModel

admin.site.register(TierModel, TierModelAdmin)