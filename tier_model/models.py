from django.db import models
from staff.models import Staff
from completed_work_period.models import CompletedWorkPeriod
from active_work_period.models import ActiveWorkPeriod
from django.db.models.signals import post_save, pre_delete
class TierModel(models.Model):
    """
    Model for storing information about tiers
        fields:
            name -> this is title of tier
            address -> adress of this tier with city
            staff -> array of staff that works in this tier (ManyToMany)
            active_work_period -> active work period (OneToOne)
            compleded_work_period -> compleded work period (ManyToMany)
            weapon_room -> weapon room (OneToOne)
            stock-> stock (OneToOne)
        additional funcs:
            post_save: checks if weapon room and stocks have relations to this tier
            pre_delete: checks that after deleting tier  weaponroom and stocks have no relations with this tier

    """
    class Meta:
        db_table = "TierModel"
        verbose_name = "Тир"
        verbose_name_plural = "Тиры"
        permissions = (
            ("view_tier_model", "Can view Тиры"),
        )

    name = models.CharField(verbose_name="Название", max_length=100, blank=False, null=False, unique=True)
    address = models.CharField(verbose_name="Адрес", max_length=100, blank=False, null=False)
    staff = models.ManyToManyField(Staff, verbose_name="Сотрудник", blank=False)
    active_work_period = models.OneToOneField(ActiveWorkPeriod, verbose_name="Активные смены", blank=True, on_delete=models.SET_NULL, null=True)
    compleded_work_period = models.ManyToManyField(CompletedWorkPeriod, verbose_name="Завершенные смены", blank=True )
    weapon_room = models.OneToOneField('weapon_room.WeaponRoom', verbose_name="Оружейная", max_length=100, blank=False, null=True, on_delete=models.SET_NULL)
    stock = models.OneToOneField('stock.Stock', verbose_name="Склад", blank=False, null=True, on_delete=models.SET_NULL )

    @staticmethod
    def post_save(sender, instance, *args, **kwargs):
        """
        checks if weapon room and stocks have relations to this tier, 
        if no: 
            adds relations to this tier
        """
        if instance.weapon_room is not None:
            weapon_room = instance.weapon_room
            if hasattr(weapon_room, 'tier'): 
                weapon_room.tier = instance
                weapon_room.save()
        if instance.stock is not None:
            stock = instance.stock
            if hasattr(stock, 'tier'): 
                stock.tier = instance
                stock.save()

    @staticmethod
    def pre_delete(sender, instance, *args, **kwargs):
        if instance.weapon_room == None:
            return
        weapon_room = instance.weapon_room
        weapon_room.tier = None
        weapon_room.save()
        if instance.stock == None:
            return
        stock = instance.stock
        stock.tier = None
        stock.save()

    def __str__(self):
        return '{NAME} {ADDRESS} {WEAPON_ROOM} {STOCK} {STAFF}'.format(
            NAME=self.name,
            ADDRESS=self.address,
            WEAPON_ROOM=self.weapon_room,
            STOCK=self.stock,
            STAFF=self.staff,
        )

post_save.connect(TierModel.post_save, sender=TierModel)
pre_delete.connect(TierModel.pre_delete, sender=TierModel)
