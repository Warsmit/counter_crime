from django.apps import AppConfig


class TierModelConfig(AppConfig):
    name = 'tier_model'
