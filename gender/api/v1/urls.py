from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .viewsets import (
    GenderList,
    GenderDetail,
)


urlpatterns = [
    url(r'^genders/$', GenderList.as_view(), name='gender-list'),
    url(r'^genders/(?P<pk>[0-9]+)$', GenderDetail.as_view(), name='gender-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
