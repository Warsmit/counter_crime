#!/bin/bash

function usage() {
    echo "Usage: $0 [port_value]"
}
VERSION="0.0.1"

. common_bash_commands

case "$OSTYPE" in
    msys)
        PYTHON=python
        ;;
    linux-gnueabihf)
        PYTHON=python3.8
        ;;
    linux-gnu)
        PYTHON=python3
        ;;
    *)
        usage
    ;;
esac

prepare
activateEnviroment
$PYTHON manage.py runserver 127.0.0.1:${1:-8000}