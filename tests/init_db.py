# runserver for create db
# delete all migrations folder in the project
# run makemigrations for each app in the backend project
# run migrate
# create super user admin , login as admin, and set token in fill_db.py
# run server
from base64 import b64encode
import os
import requests
import json

server_url = "http://127.0.0.1:8000/"
client = requests.session()


def upload_data(data, url, file=None):
    for d in data:
        # d = json.dumps(d)
        result = client.post(url, data=d, headers=headers, files=file,
                             cookies=client.cookies)
        print(result.status_code)
        print(result.json())


login_url = server_url + "rest-auth/login/"
registation_url = server_url + "rest-auth/registration/"
group_url = server_url + "api/v1/groups/"
user_url = server_url + "api/v1/users/"
gender_url = server_url + "api/v1/genders/"
pasport_url = server_url + "api/v1/pasports/"
weapon_type_url = server_url + "api/v1/weapon_types/"
weapon_url = server_url + "api/v1/weapons/"
registration_place_url = server_url + "api/v1/registration_places/"
client_model_url = server_url + "api/v1/client_models/"
active_transaction_url = server_url + "api/v1/active_transactions/"
ammo_type_url = server_url + "api/v1/ammo_types/"
# completed_transaction = server_url + "api/v1/completed_transactions/"
weapon_room_url = server_url + "api/v1/weapon_rooms/"
stocks_url = server_url + "api/v1/stocks/"
tier_url = server_url + "api/v1/tiers/"
weapon_state_url = server_url + "api/v1/weapon_states/"
staff_url = server_url + "api/v1/staffs/"
staff_position_url = server_url + "api/v1/staff_positions/"
med_certificate_url = server_url + "api/v1/med_certificates/"
active_work_period_url = server_url + "api/v1/active_work_periods/"
weapon_permit_url = server_url + "api/v1/weapon_permits/"
target_model_url = server_url + "api/v1/targets/"

user = {
    "username": "admin",
    "email": '',
    "password": "asdasd"
}


groups = [
    {"name": "Admin"},
    {"name": "Moderator"},
    {"name": "Client"},
]

users = [
    {"username": "guest_1", "email": "guest_1@susanin.com", "password1": "reshetochka",
        "password2": "reshetochka",  "first_name": "Александр", "phone_number": "89601840935",
        "last_name": "Мамонов", "second_name": "Евгеньевич", 
        "group": groups[2]['name'],
    },
    {"username": "moderator_2", "email": "moderator_2@susanin.com", "first_name": "Александр",
        "last_name": "Мамонов", "second_name": "Евгеньевич","phone_number": "89601840936",
        "password1": "reshetochka", "password2": "reshetochka",
    "group": groups[1]['name']
    },
    {"username": "guide_3",
        "email": "guide_3@susanin.com",
        "password1": "reshetochka",
        "first_name": "Александр",
        "last_name": "Мамонов",
        "phone_number": "89601840937",
        "second_name": "Евгеньевич",
        "password2": "reshetochka",
        "group": groups[2]['name']
    },
]

result = client.post(login_url, data=user).content.decode('utf-8')
token = json.loads(result).get('key')


headers = {
    "Authorization": "Token " + token,
    'X-CSRFToken': client.cookies['csrftoken'],
    # 'Content-Type': 'application/json',
    'charset': 'utf-8'
}

registation_places = [
    {
        "region": "Нижегородская область",
        "township": "Nizhny Novgorod",
        "street": "Ulitsa Belinskogo",
        "house_number": 123,
        "building": 23,
        "apartment": 1233123,
    }
]

pasports = [
    {
        "series": 1234,
        "number": 123456,
        "issuer_date": "2020-07-30",
        "issued_by": "asd",
        "department_code": "345-345",
        "first_name": "Александр",
        "last_name": "Мамонов",
        "second_name": "Мамонов",
        "birth_date": "1999-06-10",
        "gender": gender_url + "1",
        "registration_place": registration_place_url + "1",
    }
]

genders = [
    {
        "name": "муж"
    },
    {
        "name": "жен"
    }
]
weapon_types = [
    {"name": "Травматическое"}
]

ammo_types = [
    {
        "caliber": "7,65",
        "marker": "Красный",
        "count": 100,
    }
]

weapon_states = [
    {
        "state": "работает"
    },
    {
        "state": "в ремонте"
    },
]

weapons = [
    {
        "weapon_name": "Глок",
        "serial_number": "345-ппо",
        "weapon_types": weapon_type_url + "1",
        "ammo_type": ammo_type_url + "1",
        "state": weapon_state_url + "1",
    }
]

clients = [
    {
        "phone_number": "8800553535",
        "user": user_url + "2",
        "pasport": pasport_url + "1",
        "allowed_weapons": [
            weapon_url + "1"
        ],
    }
]

med_sertificates = [
    {
        "client": client_model_url + '1',
        "number": 123123,
        "issuer_date": "2020-07-30",
    }
]

image_file = {"image": open('tests/test.png', 'rb')}
med_certificate_image = {"image": open('tests/med_certificate.jpg', 'rb')}
image_file3 = {"image": open('tests/test.png', 'rb')}
weapon_permit_image = {"image": open('tests/weapon_permit.jpg', 'rb')}
weapon_rooms = [
    {
        "name": "Оружейная комната 1",
        "weapons": [
            weapon_url + "1"
        ],
    }
]

stoks = [
    {
        "name": "Склад 1",
        "weapons": [
            weapon_url + "1"
        ],
        "ammo": [
            ammo_type_url + "1"
        ]
    }
]

active_transactions = [
    {
        "expire_time": "00:00:13",
        "client": client_model_url + "1",
        "weapon": weapon_url + "1",
        "tier": tier_url + "1"
    }
]


tiers = [
    {
        "name": "321",
        "address": "нижний",
        "weapon_room": weapon_room_url + "1",
        "stock": stocks_url + "1",
        "staff": [
            staff_url + "1"
        ],
        "active_work_period" : active_work_period_url + "1"
        
    }
]

staff_positions = [
    {
        "name": "оружейник",
    }
]


staffs = [
    {
        "phone_number": "88005553535",
        "user": user_url + "3",
        "staff_position": staff_position_url + "1",
    }
]

active_work_periods = [
    {
        "staff": [
        staff_url + "1"
        ],
        "active_transaction": [],
        "completed_transaction": [],
    }
]

weapon_permits = [
    {
        "client": client_model_url + '1',
        "number": 123123,
        "issuer_date": "2020-07-30",
    }
]

targets = [
    {
    "result": "34124",
    }
]

upload_data(users, registation_url)
upload_data(registation_places, registration_place_url)
upload_data(genders, gender_url)
upload_data(pasports, pasport_url)
upload_data(weapon_states, weapon_state_url)
upload_data(weapon_types, weapon_type_url)
upload_data(ammo_types, ammo_type_url)
upload_data(weapons, weapon_url)
upload_data(weapon_rooms, weapon_room_url)
upload_data(clients, client_model_url, file=image_file)
upload_data(stoks, stocks_url)
upload_data(staff_positions, staff_position_url)
upload_data(staffs, staff_url, file=image_file3)
upload_data(active_work_periods, active_work_period_url)
upload_data(tiers, tier_url)
upload_data(med_sertificates, med_certificate_url,file=med_certificate_image)
upload_data(active_transactions, active_transaction_url)
upload_data(weapon_permits, weapon_permit_url, file=weapon_permit_image)
upload_data(targets, target_model_url)