from django.apps import AppConfig


class StaffPositionConfig(AppConfig):
    name = 'staff_position'
