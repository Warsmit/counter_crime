from django.contrib import admin
from .models import StaffPosition

class StaffPositionAdmin(admin.ModelAdmin):
    list_display = [field.name for field in StaffPosition._meta.fields]

    class Meta:
        model = StaffPosition

admin.site.register(StaffPosition, StaffPositionAdmin)
