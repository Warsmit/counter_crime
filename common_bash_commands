#!/bin/bash

function returnError() {
    echo "Error: " $*
    exit 1;
}

function checkModule() {
    if ! [ -x "$(command -v $1)" ]; then
        returnError "$1 is not installed"
    fi
}

function createEnviroment() {
    # Create ENV
    echo "Building... in env - venv"
    $PYTHON -m venv venv
    echo "Created virtual environment: venv"
}

function activateEnviroment() {
    # Activate VENV
    if [ $PYTHON = "python" ]; then
        source venv\\Scripts\\activate
    elif [[ $PYTHON  == "python3" ]]; then
        source venv/bin/activate
    elif [[ $PYTHON  == "python3.8" ]]; then
        source venv/bin/activate
    fi
    echo "Activated virtual environment: venv"
}

function upgradePip() {
    # upgrade pip
    $PIP install --upgrade pip
}

function installServerModules() {
    # Install Server dependencies
    $PIP install -r requirements.txt
    echo "Done! Check for errors"
}

function prepare() {
    checkModule $PYTHON
}

function migrate() {
    removeMigrations
    $PYTHON manage.py makemigrations user
    $PYTHON manage.py migrate
    $PYTHON manage.py migrate --run-syncdb
}

function setup() {
    upgradePip
    installServerModules
}

function build() {
    prepare
    createEnviroment
    activateEnviroment
    setup
}

function removeMigrations() {
    for i in $(find . -name "migrations" -not -path "./venv/*"); do
        rm -rf $i
    done
}
