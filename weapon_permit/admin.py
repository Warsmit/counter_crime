from django.contrib import admin
from .models import WeaponPermit
from django.utils.safestring import mark_safe

class WeaponPermitAdmin(admin.ModelAdmin):
    list_display = ('client', 'number', 'issuer_date', 'get_image', )
    class Meta:
        model = WeaponPermit

    def get_image(self, obj):
        return mark_safe(f'<img src={obj.image.url} width="220" height"220"')


admin.site.register(WeaponPermit, WeaponPermitAdmin)
