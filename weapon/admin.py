from django.contrib import admin
from .models import Weapon

class WeaponAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Weapon._meta.fields]

    class Meta:
        model = Weapon

admin.site.register(Weapon, WeaponAdmin)