from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .viewsets import  (WeaponList, WeaponDetail)

urlpatterns = [
    url(r'^weapons/$', WeaponList.as_view(), name='weapons-list'),
    url(r'^weapons/(?P<pk>[0-9]+)$', WeaponDetail.as_view(), name='weapon-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)