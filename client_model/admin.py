from django.contrib import admin
from .models import ClientModel
from django.utils.safestring import mark_safe

class ClientModelAdmin(admin.ModelAdmin):
    # list_display = [field.name for field in ClientModel._meta.fields]
    list_display = ['user', 'pasport', 'get_image']
    class Meta:
        model = ClientModel

    def get_image(self, obj):
        return mark_safe(f'<img src={obj.image.url} width="220" height"220"')

admin.site.register(ClientModel, ClientModelAdmin)
