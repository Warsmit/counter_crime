from rest_framework import serializers
from client_model.models import ClientModel

class ClientModelSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ClientModel
        fields = '__all__'

    image = serializers.ImageField(required=False,max_length=None, use_url=True)
