from django.apps import AppConfig


class ClientModelConfig(AppConfig):
    name = 'client_model'
