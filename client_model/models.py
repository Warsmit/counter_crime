from django.db import models
from pasport.models import Pasport
from user.models import User
from weapon.models import Weapon
from django.db.models.signals import pre_save
from fernet_fields import EncryptedCharField
from counter_crime.settings import IMAGE_STORAGE
from utils.rename_image import rename_client_images
from django.db.models.signals import post_delete

class ClientModel(models.Model):
    """
    Model for storing information about clients
        fields:
            user -> user (OneToOne)
            pasport -> pasport (OneToOne)
            phone_number -> client`s phone number
            allowed_weapons -> array of allowed weapon foor this client (ManyToMany)
            image -> client`s photo
        additional funcs:
            pre_save: fill in user data from passport
            delete_image: delete client`s image

    """
    class Meta:
        db_table = "Client"
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"
        permissions = (
            ("view_client", "Can view Клиенты"),
        )

    user = models.OneToOneField(User, on_delete=models.CASCADE,  blank=False)
    pasport = models.OneToOneField(
        Pasport, on_delete=models.CASCADE, verbose_name="Паспорт", blank=False)
    allowed_weapons = models.ManyToManyField(Weapon, related_name='Weapons', blank=True)
    image = models.ImageField(verbose_name="Фото Клиента", storage=IMAGE_STORAGE, max_length=500, blank=True, upload_to=rename_client_images)
    #visited_tiers = models.ManyToManyField('tier_model.TierModel', related_name='Посещенные тиры', blank=False)
    # TODO STUDY_INFO

    def __str__(self):
        return '{} {} {} {}'.format(self.user.email, self.user.last_name, self.user.first_name,  self.user.second_name, )

    @staticmethod
    def pre_save(sender, instance, *args, **kwargs):
        instance.user.birthday = instance.pasport.birth_date
        instance.user.first_name = instance.pasport.first_name
        instance.user.last_name = instance.pasport.last_name
        instance.user.second_name = instance.pasport.second_name
        instance.user.save()

    @staticmethod
    def delete_image(sender, instance, *args, **kwargs):
        if instance.image:
            print(instance.image.path)
            if os.path.isfile(instance.image.path):
                os.remove(instance.image.path)

post_delete.connect(sender=ClientModel, receiver=ClientModel.delete_image)
pre_save.connect(ClientModel.pre_save, sender=ClientModel)