from .serializers import CompletedTransactionSerializer
from completed_transaction.models import CompletedTransaction
from user.permissions import ExtendedModelPermissionsNonAuthReadOnly
from rest_framework.filters import SearchFilter
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import (
    generics,
    permissions
)


class CompletedTransactionList(generics.ListCreateAPIView):
    queryset = CompletedTransaction.objects.all()
    serializer_class = CompletedTransactionSerializer

    filter_backends = (
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter,
    )

    filter_fields = '__all__'
    search_fields = '__all__'
    ordering_fields = '__all__'

    permission_classes = [
        # permissions.IsAuthenticatedOrReadOnly,
        ExtendedModelPermissionsNonAuthReadOnly
        # permissions.DjangoModelPermissionsOrAnonReadOnly,
        # ExtendedModelPermissions
    ]


class CompletedTransactionDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = CompletedTransaction.objects.all()
    serializer_class = CompletedTransactionSerializer

    permission_classes = [
        # permissions.IsAuthenticated,
        # permissions.IsAuthenticatedOrReadOnly,
        ExtendedModelPermissionsNonAuthReadOnly
        # ExtendedModelPermissions
    ]
