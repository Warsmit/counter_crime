from django.contrib import admin
# from django.contrib.auth.admin import UserAdmin
from .models import User
from .models import PhoneOTP

class UserAdmin(admin.ModelAdmin):
    list_display = [field.name for field in User._meta.fields]

    class Meta:
        model = User

class PhoneOTPAdmin(admin.ModelAdmin):
    list_display = [field.name for field in PhoneOTP._meta.fields]

    class Meta:
        model = PhoneOTP

admin.site.register(User, UserAdmin)
admin.site.register(PhoneOTP)