from django.db import models
from weapon.models import Weapon
from client_model.models import ClientModel
from django.db.models.signals import post_save
from django.core.validators import ValidationError
class ActiveTransaction(models.Model):
    """
    Model for storing active transactions
        fields:
            client -> client (OneToOne)
            weapon -> type of weapon (Weapon)
            expire_time -> duration time HH:MM:SS
            created -> date and time when the transaction was created
            tier -> title of tier
        additional funcs:
            post_save: checks relations between this tier and active work period

    """
    class Meta:
        db_table = "ActiveTransaction"
        verbose_name = "Список активных Транзакций"
        verbose_name_plural = "Списки активных Транзакций"
        permissions = (
            ("view_active_transaction", "Can view Списки активных транзакций"),
        )

    client = models.OneToOneField(ClientModel, verbose_name="Клиент",
                                  related_name="User", on_delete=models.CASCADE, blank=False, null=True)
    weapon = models.ForeignKey(Weapon, related_name="Weapon",
                               on_delete=models.CASCADE, blank=True, null=False, max_length=100)
    expire_time = models.TimeField(
        verbose_name="Срок действия в формате {ЧЧ:ММ:СС}", blank=False, null=False, )
    created = models.DateTimeField(
        verbose_name="Время начала транзакции", auto_now_add=True, auto_now=False)
    tier = models.ForeignKey("tier_model.TierModel", verbose_name="Название тира", blank=False, on_delete=models.CASCADE, null=False)

    def __str__(self):
        return '{CLIENT} {WEAPON} {EXPIRE_TIME}'.format(
            WEAPON=self.weapon,
            CLIENT=self.client,
            EXPIRE_TIME=self.expire_time
        )

    @staticmethod
    def post_save(sender, instance, created, *args, **kwargs):
        """
        check if this tier have active work period,
        if no:
            raise an exception
        """
        if created:
            tier = instance.tier
            if tier.active_work_period is not None:
                tier.active_work_period.active_transaction.add(instance)
            else:
                raise ValidationError('У выбранного тира нет активной смены')

post_save.connect(ActiveTransaction.post_save, sender=ActiveTransaction)