from django.apps import AppConfig


class ActiveTransactionConfig(AppConfig):
    name = 'active_transaction'
