from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .viewsets import  (ActiveTransactionList, ActiveTransactionDetail)

urlpatterns = [
    url(r'^active_transactions/$', ActiveTransactionList.as_view(), name='activetransaction-list'),
    url(r'^active_transactions/(?P<pk>[0-9]+)$', ActiveTransactionDetail.as_view(), name='activetransaction-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)