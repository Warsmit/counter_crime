from rest_framework import serializers
from active_transaction.models import ActiveTransaction

class ActiveTransactionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ActiveTransaction
        fields = '__all__'