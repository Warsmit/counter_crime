from django.db import models
from registration_place.models import RegistrationPlace
from django.core.validators import RegexValidator, ValidationError
from utils.validators import DepartamentCodeValidator, NameValidator, BirthDateValidator
from gender.models import Gender
from datetime import date, timedelta
from fernet_fields import (
    EncryptedCharField,
    EncryptedDateField,
    EncryptedIntegerField,
    )
from utils.get_year import get_year

# Create your models here.
class Pasport(models.Model):
    """
    Model for storing passport information
        fields:
            series -> serial number (4 digits)
            number -> passport number (6 dijits)
            birth_date -> date of birth
            issuer_date -> date of issue of the passport
            department_code -> code of department where was passport issued
            first_name -> first name
            second_name -> second name
            gender -> gender
            registration_place -> registration place
            created -> date and time of creation
            updated -> date and time of updation
        additional funcs:
            save: save passport information and checking the validity of a passport
            validate_pasport_issuer_date: checking the validity of a passport

    """
    class Meta:
        db_table = "Pasport"
        verbose_name = "Паспорт"
        verbose_name_plural = "Паспорта"
        permissions = (
            ("view_pasports", "Can view Паспорта"),
        )

    series = EncryptedIntegerField(verbose_name="Серия", blank=False, null=False, validators=[RegexValidator(regex='^.{4}$', message='Поле должно содержать 4 цифры', code='nomatch')])
    number = EncryptedIntegerField(verbose_name="Номер", blank=False, null=False, validators=[RegexValidator(regex='^.{6}$', message='Поле должно содержать 6 цифр', code='nomatch')])
    birth_date = EncryptedDateField(verbose_name="Дата Рождения", blank=False, null=False, validators=[BirthDateValidator()])
    issuer_date = EncryptedDateField(verbose_name="Дата Выдачи", blank=False, null=False,)
    issued_by = EncryptedCharField(verbose_name="Кем выдан", max_length=1300, blank=False, null=False,)
    department_code = EncryptedCharField(verbose_name="Код Подразделения",  blank=False, null=False, max_length=7, validators=[DepartamentCodeValidator()])
    first_name = EncryptedCharField(verbose_name="Имя", max_length=100, blank=False, null=False, validators=[NameValidator()])
    last_name = EncryptedCharField(verbose_name="Фамилия", max_length=100, blank=False, null=False, validators=[NameValidator()])
    second_name = EncryptedCharField(verbose_name="Отчетство", max_length=100, blank=False, null=False, validators=[NameValidator()])
    gender = models.ForeignKey(Gender, verbose_name="Пол", blank=False, null=True, on_delete=models.SET_NULL)
    registration_place = models.OneToOneField(RegistrationPlace, on_delete=models.SET_NULL, verbose_name="Место Регистрации", blank=False, null=True)
    created = models.DateTimeField(verbose_name="Created", auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(verbose_name="Updated", auto_now_add=False, auto_now=True)


    def save(self, *args, **kwargs):
        if self.issuer_date <= self.birth_date:
            raise ValidationError("Не корректная дата выдачи паспорта")
        age = get_year((date.today() - self.birth_date))
        if age >= 14 and age<20: 
            self.validate_pasport_issuer_date(14)
        elif age >= 20 and age < 45:
            self.validate_pasport_issuer_date(20)
        elif age >= 45 :
            self.validate_pasport_issuer_date(45)
        super().save(*args, **kwargs)

    def validate_pasport_issuer_date(self, age):
        passport_expire_date = self.birth_date.replace(year=self.birth_date.year + age)
        if self.issuer_date < passport_expire_date:
            raise ValidationError("Не корректная дата выдачи паспорта, паспорт нужно менять после {} лет".format(age))

    def __str__(self):
        return '{SERIES} {NUMBER} {ISSUER_DATE} {DEPARTAMENT_CODE} {FIRST_NAME} {LAST_NAME} {SECOND_NAME} {BIRTH_DATE}'.format(
            SERIES=self.series, 
            NUMBER=self.number,
            ISSUER_DATE=self.issuer_date,
            DEPARTAMENT_CODE=self.department_code,
            FIRST_NAME=self.first_name,
            LAST_NAME=self.last_name,
            SECOND_NAME=self.second_name,
            BIRTH_DATE=self.birth_date,
        )
