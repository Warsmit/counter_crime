from .serializers import PasportSerializer
from pasport.models import Pasport
from user.permissions import ExtendedModelPermissionsNonAuthReadOnly
from rest_framework.filters import SearchFilter
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import (
    generics,
    permissions
)

class PasportList(generics.ListCreateAPIView):
    queryset = Pasport.objects.all()
    serializer_class = PasportSerializer

    filter_backends = (
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter,
    )

    filter_fields = '__all__'
    search_fields = '__all__'
    ordering_fields = '__all__'

    permission_classes = [
        # permissions.IsAuthenticatedOrReadOnly,
        ExtendedModelPermissionsNonAuthReadOnly
        # permissions.DjangoModelPermissionsOrAnonReadOnly,
        # ExtendedModelPermissions
    ]


class PasportDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Pasport.objects.all()
    serializer_class = PasportSerializer

    permission_classes = [
        # permissions.IsAuthenticated,
        # permissions.IsAuthenticatedOrReadOnly,
        ExtendedModelPermissionsNonAuthReadOnly
        # ExtendedModelPermissions
    ]