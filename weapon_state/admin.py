from django.contrib import admin
from .models import WeaponState

class WeaponStateAdmin(admin.ModelAdmin):
    list_display = [field.name for field in WeaponState._meta.fields]

    class Meta:
        model = WeaponState

admin.site.register(WeaponState, WeaponStateAdmin)