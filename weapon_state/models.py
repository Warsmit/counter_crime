from django.db import models

class WeaponState(models.Model):
    """
    Model for storing weapon states
        fields:
            state -> state of the weapon

    """
    class Meta:
        db_table = "WeaponState"
        verbose_name = "Состояние Оружия"
        verbose_name_plural = "Состояния Оружий"
        permissions = (
            ("view_weapon_state", "Can view Состояния Оружий"),
        )

    state = models.CharField(verbose_name="Состояние Оружия", blank=False, null=False, max_length=100)

    def __str__(self):
        return "{STATE}".format(
            STATE=self.state,
        )
