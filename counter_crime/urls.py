"""counter_crime URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls import url, include
import re
from rest_framework_swagger.views import get_swagger_view
from rest_auth.registration.views import VerifyEmailView, RegisterView
from rest_auth.views import (
    LogoutView,
    UserDetailsView,
    PasswordChangeView,
    PasswordResetView,
    PasswordResetConfirmView
)
import allauth
from user.views import (
    ExtendedLoginView,
    ExtendedRegisterView,
)
from user.otp_functions import otp_match
from django.views.static import serve
from client_model.api.v1.viewsets import get_client_birthdays
from staff.api.v1.viewsets import get_staff_birthdays
from active_work_period.api.v1.viewsets import close_work_period
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # Docs urls
    url(r'^docs/$', get_swagger_view(title='API Docs'), name='api_docs'),
    # rest api
    url(r'^rest-auth/password/reset/$',
        PasswordResetView.as_view(), name='rest_password_reset'),
    url(r'^rest-auth/password/reset/confirm/$',
        PasswordResetConfirmView.as_view(), name='rest_password_reset_confirm'),
    url(r'^rest-auth/login/$', ExtendedLoginView.as_view(),
        name='rest_login'),  # TODO !

    # URLs that require a user to be logged in with a valid session / token.
    url(r'^rest-auth/logout/$', LogoutView.as_view(), name='rest_logout'),
    url(r'^rest-auth/user/$', UserDetailsView.as_view(), name='rest_user_details'),
    url(r'^rest-auth/password/change/$',
        PasswordChangeView.as_view(), name='rest_password_change'),
    url(r'^rest-auth/account-confirm-email/', VerifyEmailView.as_view(),
        name='account_email_verification_sent'),
    url(r'^rest-auth/account-confirm-email/(?P<key>[-:\w]+)/$',
        VerifyEmailView.as_view(), name='account_confirm_email'),
    url(r'^api/v1/', include('user.urls')),
    # Registrations urls

    url(r'^rest-auth/registration/',
        ExtendedRegisterView.as_view(), name='rest_register'),

    # custom urls
    url(r'^api/v1/', include('pasport.api.v1.urls')),
    url(r'^api/v1/', include('weapon_type.api.v1.urls')),
    url(r'^api/v1/', include('weapon.api.v1.urls')),
    url(r'^api/v1/', include('registration_place.api.v1.urls')),
    url(r'^api/v1/', include('client_model.api.v1.urls')),
    url(r'^api/v1/', include('active_transaction.api.v1.urls')),
    url(r'^api/v1/', include('completed_transaction.api.v1.urls')),
    url(r'^api/v1/', include('weapon_room.api.v1.urls')),
    url(r'^api/v1/', include('ammo_type.api.v1.urls')),
    url(r'^api/v1/', include('med_certificate.api.v1.urls')),
    url(r'^api/v1/', include('gender.api.v1.urls')),
    url(r'^api/v1/', include('stock.api.v1.urls')),
    url(r'^api/v1/', include('tier_model.api.v1.urls')),
    url(r'^api/v1/', include('weapon_state.api.v1.urls')),
    url(r'^api/v1/', include('staff.api.v1.urls')),
    url(r'^api/v1/', include('staff_position.api.v1.urls')),
    url(r'^api/v1/', include('active_work_period.api.v1.urls')),
    url(r'^api/v1/', include('completed_work_period.api.v1.urls')),
    url(r'^api/v1/', include('weapon_permit.api.v1.urls')),
    url(r'^api/v1/', include('target_model.api.v1.urls')),

    # custom functions
    url(r'^get_client_birthdays/', get_client_birthdays,
        name="get_client_birthdays"),
    url(r'^get_staff_birthdays/', get_staff_birthdays, name="get_staff_birthdays"),
    url(r'^close_work_period/', close_work_period, name="close_work_period"),
    # url(r'^send_otp/', send_otp_this, name="send_otp_this"),
    url(r'^otp_match/', otp_match, name="otp_match"),

    url(r'^accounts/', include('allauth.urls')),
    url(r'^{}(?P<path>.*)$'.format(re.escape(settings.STATIC_URL.lstrip('/'))),
        serve, dict(document_root=settings.STATICFILES_DIRS)),
    url(r'^{}(?P<path>.*)$'.format(re.escape(settings.MEDIA_URL.lstrip('/'))),
        serve, dict(document_root=settings.MEDIA_ROOT)),
]
