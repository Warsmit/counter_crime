from django.contrib import admin
from .models import Stock
class StockAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Stock._meta.fields]
    readonly_fields = ['tier']

    class Meta:
        model = Stock


admin.site.register(Stock, StockAdmin)
